from flask import Flask
from events import Events

app = Flask(__name__)

app.add_url_rule('/', 
	view_func=Events.as_view('events'), 
	methods=['GET'])

if __name__ == '__main__':
	app.run(debug = 'TRUE')
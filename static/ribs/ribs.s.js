angular.module('RibsEventsModule')

    .factory('Ribs', [
        'Ribs.Model',
        'Ribs.Collection',

        function(model, collection) {

            return Ribs;
        }
    ])  
    .factory('Ribs.Model', [
        '$http',

        function($http) {
            Ribs.Model.prototype.url = '';

            Ribs.Model.prototype.get = function(key) {
                // TODO: should be able to return an array of models
                if (typeof key === 'undefined' || key === null) {
                    return this.model;
                } else {
                    if (typeof this.model === 'undefined' || this.model === null) {
                        return false;
                    } else {
                        return this.model[key];
                    }
                }
            };


            Ribs.Model.prototype.set = function() {
                var key,
                    value,
                    newModel;

                // TODO: extend with certain properties rather than override
                if (arguments.length > 1) {
                    key = arguments[0];
                    value = arguments[1];
                    this.model[key] = value;
                } else {
                    newModel = arguments[0]
                    if (newModel.model) newModel = newModel.model;

                    if (!this.model) {
                        this.model = newModel;
                    } else {
                        this.model = Ribs.equip.extend(this.model, newModel);
                    }
                }
            };


            Ribs.Model.prototype.save = function(options) {

                // TODO.c: raghav make this url scheme more subtle, including checking the
                // model for the id rather than just 'this'
                var self = this,
                    updatedUrl = this.url + (this.id || this.model.id);

                // console.log('[debug Model#save] this.url: ', this.url);
                // console.log('[debug Model#save] this.id: ', this.id);
                // console.log('[debug Model#save] this: ', this);
                $http
                    .put(updatedUrl, this.model)
                    .success(function(data, status, headers, config) {
                        if (options.success) options.success(data);
                    })
                    .error(function(error, status, headers, config) {
                        if (options.error) options.error(error);
                    });

                return $http;
            };  


            Ribs.Model.prototype.subscribe = function(event, method) {
                window.addEventListener(event, method);
            };

            return Ribs.Model;
        }
    ])
    .factory('Ribs.Collection', [
        '$http',

        function($http) {
            Ribs.Collection.prototype.models = [];
            Ribs.Collection.prototype.modelsIndex = {};
            Ribs.Collection.prototype.primaryKey = 'id';
            Ribs.Collection.prototype.url = '';
            Ribs.Collection.prototype.change = 0;
            Ribs.Collection.prototype.state = {};


            Ribs.Collection.prototype.setBaseUrl = function() {
                this.url = baseUrl;
            };


            Ribs.Collection.prototype.setPrimaryKey = function() {
                this.primaryKey = primaryKey;
            };


            Ribs.Collection.prototype.fetch = function(options) {
                var self = this;

                $http
                    .get(this.url, {
                        params: options.params
                    })
                    .success(function(data, status, headers, config) {
                        self.add(data.results);
                        if (options.success) options.success(self.models);
                    })
                    .error(function(error, status, headers, config) {
                        if (options.error) options.error(error);
                    });
            };


            Ribs.Collection.prototype.add = function(data) {
                if (!(data instanceof Array)) data = [ data ];
                for (var index = 0; index < data.length; index++) {
                    this.models.push(data[index]);
                    this.modelsIndex[data[index][this.primaryKey]] = index;
                }

                this.emit('ribs.add', data); 
            };


            Ribs.Collection.prototype.get = function(id) {
                ß
                // TODO: should be able to return an array of models
                if (typeof id === 'undefined' || id === null) return;
                return this.models[this.modelsIndex[id]];
            };


            Ribs.Collection.prototype.remove = function(id) {
                var index = this.modelsIndex[id]
                  , results;

                this.models.splice(index, 1);
                delete this.modelsIndex[id];

                results = this.models;

                this.reset();
                this.add(results);

                return this.models;
            };


            Ribs.Collection.prototype.reset = function() {
                this.models = [];
                this.modelsIndex = {};
            };


            Ribs.Collection.prototype.watch = function() {
                return this.change;
            };


            Ribs.Collection.prototype.changed = function() {
                this.change++;
            };


            // TODO: turn this into a finite state machine with easy transitions
            Ribs.Collection.prototype.state = function(key) {
                if (typeof(key) === 'undefined' || key === null) {
                    return state;
                }
                return state[key];
            };


            Ribs.Collection.prototype.emit = function(eventName, options) {
                var self = this;

                // create custom event
                var event = new CustomEvent(eventName, { 
                    detail: {
                        data: options || {},
                        sender: self
                    },
                    cancelable: true, 
                    bubbles: true 
                });

                //TODO: check if options contains values for cancelable and bubbles provided by user

                // trigger event
                window.dispatchEvent(event);   
            };


            Ribs.Collection.prototype.subscribe = function(eventName, method) {
                var self = this;
                var callback = function(event) {
                    if (self === event.detail.sender) {
                        method(event, event.detail.data);
                    }
                };
                window.addEventListener(eventName, callback);
            };

            return Ribs.Collection;
        }
    ])
;   

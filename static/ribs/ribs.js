var Ribs = {},
    equip;

(function(Ribs, equip) {

    var toEquip,
        breaker = {},
        Model,
        Collection;

    // TODO: write an equip service
    // TODO: change from singleton structure for the services
    // (equip abstracts this away? choose whether to initialize or not?)
    equip = function equip(model, equippables, wielder, isolate) {
        // if (!wielder || !object) return this;

        // TODO: switch to inheritance using coffeescript prototyping?
        // TODO: steal the Backbone extend method?
        // TODO: steal my own augment method

        // TODO: add a isFacebookOnly method to light-weight friend/user object
        // if (!user) return false;

        // TODO: should this inherit instead of setting the model?

        // TODO: expose as bp-user.services?
        // TODO: provide two different modes of access? or should it be always or never...

        // c - wrote some shady new inheritance paradigm. why would i do that?!
        if (!wielder) {
            var Wielder = function() {
                // console.log('[debug equip] this: ', this);
                return this;
            };

            // XXX: note source.constructor does not point to Wielder
            // c - is this even necessary? it's kind of cool, i guess
            // let's just roll with the inheritance for now
            // notice new is not called on source because we do not want a new instance
            // of source created - we just want source to be copied to [[proto]]
            // @see http://stackoverflow.com/questions/1646698/what-is-the-new-keyword-in-javascript
            Wielder.prototype = model;
            // if (!source.constructor) = Wielder;

            // TODO: write a method to replace Wielder.prototype
            // when source becomes irrelevant
            wielder = new Wielder();
            wielder._super = model;

            // console.log('[debug equip] wielder before equipping: ', wielder);
        }

        // console.log('[debug equip] setting up equippables with source: ', model);
        for (var key in equippables) {
            if (isolate) {
                wielder[key] = new equippables[key](wielder, model);
            } else {
                // console.log('[debug equip] setting up equippable with key: ', equippables[key]);
                wielder[key] = new equippables[key](wielder, model);
            }
        }

        return wielder;
    }

    equip.inherit = function inherit(child, parent) {
        for (var key in parent) {
            if (Object.prototype.hasOwnProperty.call(parent, key)) {
                child[key] = parent[key];
            }
        }
        function ctor() { this.constructor = child; }
        ctor.prototype = parent.prototype;
        child.prototype = new ctor;
        child.__super__ = parent.prototype;
        return child;
    };

    equip.apprentice = function apprentice(source, equippables, Parent) {
        if (!Parent) return false;

        var Wielder
          , wielder;

        Wielder = function() { return Parent.apply(this, arguments); };
        Wielder = equip.inherit(Wielder, Parent);

        // console.log('[debug equip.equip] Wielder: ', Wielder);

        if (!source || !equippables) return Wielder;

        return equip(source, equippables, new Wielder(source));
    };

    equip.transform = function transform() {
        if (!Parent || !object) return this;

        var Equippable
          , Wielder
          , prototype
          , constructor;

        // TODO: switch to inheritance using coffeescript prototyping?
        // TODO: steal the Backbone extend method?
        // TODO: steal my own augment method

        // var service = new Service();

        // Child = angular.copy(Child);

        prototype = child.prototype;

        Equippable = function() { this.constructor = Child; };
        Equippable.prototype = Parent.prototype;

        Wielder = function() { return parent.apply(this, arguments); };
        Wielder.prototype = new Equippable;

        child.__proto__ = Wielder.prototype;

        return child;
    };

    equip.each = function each(obj, iterator, context) {
        if (obj == null) return;
        if (Array.prototype.forEach && obj.forEach === Array.prototype.forEach) {
            obj.forEach(iterator, context);
        } else if (obj.length === +obj.length) {
            for (var i = 0, l = obj.length; i < l; i++) {
              if (iterator.call(context, obj[i], i, obj) === breaker) return;
            }
        } else {
            for (var key in obj) {
                if (obj.hasOwnProperty(key)) {
                  if (iterator.call(context, obj[key], key, obj) === breaker) return;
                }
            }
        }
    };

    equip.extend = function extend(obj) {
        equip.each(Array.prototype.slice.call(arguments, 1), function(source) {
            if (source) {
                for (var prop in source) {
                    if (source.hasOwnProperty(prop)) {
                        obj[prop] = source[prop];
                    }
                }
            }
        });
        return obj;
    };

    equip.extendTo = function extendTo(protoProps, staticProps) {
        var parent = this;
        var child;

        // The constructor function for the new subclass is either defined by you
        // (the "constructor" property in your `extend` definition), or defaulted
        // by us to simply call the parent's constructor.
        if (protoProps && protoProps.hasOwnProperty('constructor')) {
          child = protoProps.constructor;
        } else {
          child = function(){ return parent.apply(this, arguments); };
        }

        // Add static properties to the constructor function, if supplied.
        equip.extend(child, parent, staticProps);

        // Set the prototype chain to inherit from `parent`, without calling
        // `parent`'s constructor function.
        var Surrogate = function() { this.constructor = child; };
        Surrogate.prototype = parent.prototype;
        child.prototype = new Surrogate;

        // Add prototype properties (instance properties) to the subclass,
        // if supplied.
        if (protoProps) equip.extend(child.prototype, protoProps);

        // Set a convenience property in case the parent's prototype is needed
        // later.
        child.__super__ = parent.prototype;

        return child;
    };

    equip.mixin = function(obj) {

        // do i need this?
        // this._wrapped = {};
        equip.each(obj, function(name) {
            var func = this[name] = obj[name];
            this.prototype[name] = function() {
                // var args = [this._wrapped];
                // Array.prototype.push.apply(args, arguments);

                return func.apply(this, arguments);
                // this seems to be wrapping the functions if they're returned
                // return result.call(this, func.apply(, args));
            };
        });
    };

    equip.result = function(object, property) {
        if (object == null) return void 0;
        var value = object[property];
        return (typeof value === 'function') ? value.call(object) : value;
    };

    var Model = Ribs.Model = function Model() {
        // var defaults;
        // var attrs = attributes || {};
        // options || (options = {});
        // this.cid = _.uniqueId('c');
        // this.attributes = {};
        // _.extend(this, _.pick(options, modelOptions));
        // if (options.parse) attrs = this.parse(attrs, options) || {};
        // if (defaults = _.result(this, 'defaults')) {
        //   attrs = _.defaults({}, attrs, defaults);
        // }
        // this.set(attrs, options);

        this.initialize.apply(this, arguments);
    };

    var Collection = Ribs.Collection = function Collection() {
        data = arguments[0];
        if (typeof data === 'undefined' || data === null) {
            console.log("[debug Ribs.Collection] Models not initialized! Initializing to an empty array...");
            this.models = [];
        } else {
            this.add(data);        
        }
        this.initialize.apply(this, arguments);
    };

    Ribs.equip = equip;
    Model.methods = Ribs.Model.prototype;
    Model.prototype.initialize = function() {};

    Collection.equip = equip;
    Collection.methods = Ribs.Collection.prototype;
    Collection.prototype.initialize = function() {};

    Collection.extend = Model.extend = equip.extendTo;
    Collection.mixin = Model.mixin = equip.mixin;
    Collection.equip = Model.equip = equip;

})(Ribs, equip);

(function() {
    if (! (typeof window.CustomEvent === 'undefined' || window.CustomEvent === null)) {
        return;
    }

    try {
        var fooEvent = new CustomEvent('', {});
    } catch (err) {
        function CustomEvent(event, params) {
            params = params || { bubbles: false, cancelable: false, detail: undefined };
            var customEvent = document.createEvent('CustomEvent');
            customEvent.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
            return customEvent;
        };

        CustomEvent.prototype = window.CustomEvent.prototype;
        window.CustomEvent = CustomEvent;
    }
})(); 
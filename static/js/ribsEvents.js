angular.module('RibsEventsModule', [])
    .controller('RibsEventsModule.EventsController', [
        '$scope',
        'Ribs',
        function($scope, Ribs) {
            var FooCollection = Ribs.Collection.extend({
                baseUrl: '/foo.py',
                primaryKey: 'value'
            });

            var collection = new FooCollection();
            var collection2 = new FooCollection();
            $scope.items2 = [];

            collection.subscribe('ribs.add', function(event, data) {
                collection2.add(data);
                $scope.items2 = collection2.models;
            });

            $scope.add = function() {
                collection.add($scope.field1);
                $scope.items = collection.models;
                $scope.field1 = "";
            };
        }
    ])
;


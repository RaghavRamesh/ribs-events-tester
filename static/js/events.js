angular.module('EventsModule', [])
    .controller('EventsModule.EventsController', [
        '$scope',

        function($scope) {
            $scope.items = [{'value': 'a'}];
            $scope.items2 = [];

            var addListeners = function() {
                console.log("addListeners()");
                document.getElementById('addbutton').addEventListener('click', add, false);
                // document.getElementById('addbutton').addEventListener('click', updateTable, false);
            };

            var add = function() {
                console.log("add()")
                $scope.items.push({ 'value': $scope.field1 }); 
                $scope.field1 = "";

                console.log(JSON.stringify($scope.items));
            };    

            function updateTable() {
                $scope.items2 = $scope.items;
            };

            addListeners();   
        }
    ])
;




// var fireEvents = function() {
//     var tableUpdate = new CustomEvent('tableUpdate', {
//         detail: $scope.items,
//         bubbles: true
//     });

//     document.getElementById("addbutton").dispatchEvent(tableUpdate);

//     document.addEventListener(tableUpdate, function() {
//         $scope.items2 = $scope.items;
//     }, false);

//     console.log($scope.items2);
// };


